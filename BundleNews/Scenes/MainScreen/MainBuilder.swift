//
//  MainBuilder.swift
//  BundleNews
//
//  Created by Rooster on 21.03.2019.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//

import UIKit

final class MainBuilder {
    
    static func make() -> MainViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        return viewController
    }
}
