//
//  MainViewController.swift
//  BundleNews
//
//  Created by Rooster on 21.03.2019.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//


import UIKit
import Parchment

// Create our own custom paging view and override the layout
// constraints. The default implementation positions the menu view
// above the page view controller, but since we're going to put the
// menu view inside the navigation bar we don't want to setup any
// layout constraints for the menu view.
class CustomPagingView: PagingView {
    
    override func setupConstraints() {
        // Use our convenience extension to constrain the page view to all
        // of the edges of the super view.
        constrainToEdges(pageView)
    }
}

// Create a custom paging view controller and override the view with
// our own custom subclass.
class CustomPagingViewController: FixedPagingViewController {
    override func loadView() {
        view = CustomPagingView(
            options: options,
            collectionView: collectionView,
            pageView: pageViewController.view)
    }
}

class IndexViewController: UIViewController {
    
    init(menuTitle: String) {
        super.init(nibName: nil, bundle: nil)
        
        title = menuTitle
        view.backgroundColor = .white
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class MainViewController: UIViewController {
    
    let notificationViewController = NotificationListBuilder.make()
    
    @IBOutlet weak var a1: UIView!
    @IBOutlet weak var a2: UIView!
    @IBOutlet weak var topView: UIView!
    
    let pagingViewController = CustomPagingViewController(viewControllers: [
            NotificationListBuilder.make(),
            IndexViewController(menuTitle: "Kayıtlı Haberler")
        ])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pagingViewController.borderOptions = .hidden
        pagingViewController.menuBackgroundColor = .clear
        pagingViewController.indicatorColor = .red
        pagingViewController.indicatorOptions = .visible(
            height: 2,
            zIndex: Int.max,
            spacing: UIEdgeInsets.zero,
            insets: UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8))
        pagingViewController.menuItemSize = .fixed(width: 10, height: 40)
        pagingViewController.textColor = .black
        pagingViewController.selectedTextColor = .black
        
        // Make sure you add the PagingViewController as a child view
        // controller and contrain it to the edges of the view.
        addChild(pagingViewController)
        a2.addSubview(pagingViewController.view)
        a2.constrainToEdges(pagingViewController.view)
        pagingViewController.didMove(toParent: self)
        
        // Set the menu view as the title view on the navigation bar. This
        // will remove the menu view from the view hierachy and put it
        // into the navigation bar.
        self.navigationController?.isNavigationBarHidden = true
        a1.addSubview(pagingViewController.collectionView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        guard let navigationBar = navigationController?.navigationBar else { return }
        pagingViewController.collectionView.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: navigationBar.bounds.size)
        pagingViewController.menuItemSize = .fixed(width: 120, height: navigationBar.bounds.height)
    }
    
}
