//
//  NotificationListContract.swift
//  BundleNews
//
//  Created by Rooster on  20.03.2019.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//

import Foundation

protocol NotificationListViewModelProtocol: class {
    var delegate: NotificationListViewModelDelagate? { get set }
    func load(lastRssId: String)
}

enum NotificaitonListViewModelOutput: Equatable {
    case setLoading(Bool)
    case showNotificationList([NewsPresentation])
}

protocol NotificationListViewModelDelagate: class {
    func handleViewModelOutput(_ output: NotificaitonListViewModelOutput)
}
