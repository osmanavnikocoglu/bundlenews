//
//  NotificationListViewController.swift
//  BundleNews
//
//  Created by Rooster on 20.03.2019.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//

import UIKit
import RealmSwift

final class NotificationListViewController : UIViewController{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var viewModel: NotificationListViewModelProtocol! {
        didSet {
            viewModel.delegate = self
        }
    }
    
    private var newsList: [NewsPresentation] = []
    private var lastRsstId: String = ""
    private let refreshControl = UIRefreshControl()
    var showRefreshControl = true
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        title = "Bildirimler"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.load(lastRssId: lastRsstId)
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshNewsData(_:)), for: .valueChanged)
    }
    @objc private func refreshNewsData(_ sender: Any) {
        fetchNewsData()
    }
    private func fetchNewsData() {
        self.newsList.removeAll()
        showRefreshControl = false
        viewModel.load(lastRssId: "")
        self.refreshControl.endRefreshing()
        indicator.stopAnimating()
    }
    
}

extension NotificationListViewController: NotificationListViewModelDelagate {
    
    func handleViewModelOutput(_ output: NotificaitonListViewModelOutput) {
        
        switch output {
        case .setLoading(let isLoading):
            
            if isLoading  && showRefreshControl {
                indicator.isHidden = false
                indicator.startAnimating()
            } else {
                indicator.isHidden = true
                indicator.stopAnimating()
                showRefreshControl = true
            }
            
        case .showNotificationList(let list):
            self.newsList += list
            tableView.reloadData()
        }
    }
}



extension NotificationListViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = Bundle.main.loadNibNamed("NewsCell", owner: self, options: nil)?.first as? NewsCell , !newsList.isEmpty else {
            return UITableViewCell()
        }
    
        
        let news = newsList[indexPath.row]
        cell.configure(item: news)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if newsList.count-1 == indexPath.row {
            guard let lastItem = self.newsList.last else {return}
            lastRsstId = lastItem.rssDataId ?? ""
            viewModel.load(lastRssId: lastRsstId)
        }
    }
    
}

