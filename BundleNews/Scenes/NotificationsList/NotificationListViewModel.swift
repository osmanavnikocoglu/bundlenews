//
//  NotificationListViewModel.swift
//  BundleNews
//
//  Created by Rooster on  20.03.2019.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//

import Foundation
import RealmSwift

final class NotificationListViewModel: NotificationListViewModelProtocol {
    
    weak var delegate: NotificationListViewModelDelagate?
    private let service: BundleNewsServiceProtocol
    static var isFirstRead = true
    
    init(service: BundleNewsService) {
        self.service = service
    }
    
    func load(lastRssId: String) {
        
        notify(.setLoading(true))
        
        service.fetchBundleNews(lastRssId: lastRssId) {  [weak self] (response) in
            guard let self = self else {return}
            self.notify(.setLoading(false))
            
            switch response {
                
            case .success(let response):
                guard let data = response.data,let items = data.items  else {return}
                
                let newsDetailPresentations = items.map({ item in
                    
                    NewsPresentation(title: item.newsDetail?.title,
                                     detail: item.newsDetail?.notificationChannelCategoryLocalizationKey,
                                     time: item.newsDetail?.pubDate,
                                     rssDataId: item.newsDetail?.rssDataID)
                    
                })
                
                self.notify(.showNotificationList(newsDetailPresentations))
                
            case .failure(let error):
                if NotificationListViewModel.isFirstRead || lastRssId == "" {
                    NotificationListViewModel.isFirstRead = false
                    let realm = try! Realm()
                    let obj = realm.objects(NewsRealmPresentation.self).sorted(byKeyPath: "time", ascending: false)
                    let newsPresentations = obj.map({ item in
                        
                        NewsPresentation(title: item.title,
                                         detail: item.detail,
                                         time: item.time,
                                         rssDataId: item.rssDataId)
                        
                    })
                    let news = newsPresentations.toArray()
                    self.notify(.showNotificationList(news))
                    print(error)
                }
                
                
            }
        }     }
    
    
    private func notify(_ output: NotificaitonListViewModelOutput) {
        delegate?.handleViewModelOutput(output)
    }
    
}
public extension LazyMapCollection  {
    
    func toArray() -> [Element]{
        return Array(self)
    }
}
