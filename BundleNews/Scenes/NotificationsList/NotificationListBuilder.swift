//
//  NotificationListBuilder.swift
//  BundleNews
//
//  Created by Rooster on 20.03.2019.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//

import UIKit

final class NotificationListBuilder {
    
    static func make() -> NotificationListViewController {
        let storyboard = UIStoryboard(name: "NotificationList", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "NotificationListViewController") as! NotificationListViewController
        viewController.viewModel = NotificationListViewModel(service: app.service)
        return viewController
    }
}
