//
//  BundleNewsService.swift
//  BundleNews
//
//  Created by Rooster on 19.03.2019.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

protocol BundleNewsServiceProtocol {
    func fetchBundleNews(lastRssId: String, completion: @escaping (Result<BundleNewsResponse>) -> Void)
}

class BundleNewsService: BundleNewsServiceProtocol {
    
    public init() { }
    
    func fetchBundleNews(lastRssId: String, completion: @escaping (Result<BundleNewsResponse>) -> Void) {
        let urlString = "https://api.bundletheworld.com/v2/api/notification?countryId=228&langCode=TR&platform=2&includePictures=1&lastId=\(lastRssId)"
        
        request(urlString).responseData { (response) in
            switch response.result {
            case .success(let data):
                let decoder = Decoders.plainDateDecoder
                do {
                    let response = try decoder.decode(BundleNewsResponse.self, from: data)
                    
                    guard let data = response.data, let items = data.items else {return}
                    
                    // Çevrim dışı modda belli sayıda notificaiton tutulması düşünüldü.
                    if self.shouldDeleteRealmDatabase() {
                        self.deleteAllRealmDatabase()
                    }
                    
                    self.writeRealmDatabase(items)
                    
                    completion(.success(response))
                } catch {
                    completion(.failure(Error.serializationError(internal: error)))
                }
            case .failure(let error):
                completion(.failure(Error.networkError(internal: error)))
            }
        }
    }
    
    
    fileprivate func writeRealmDatabase(_ items: [Items]) {
        
        let realm = try! Realm()
        
        let  realmDetailModels = items.map({ item in
            NewsRealmPresentation(title: item.newsDetail?.title,
                                  detail: item.newsDetail?.notificationChannelCategoryLocalizationKey,
                                  time: item.newsDetail?.pubDate,
                                  rssDataId: item.newsDetail?.rssDataID)
        })
        
        for news in realmDetailModels {
            try! realm.write {
                realm.add(news)
            }
        }
    }
    
    fileprivate func deleteAllRealmDatabase() {
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
        
    }
    
    fileprivate func shouldDeleteRealmDatabase() -> Bool {
        let realm = try! Realm()
        let obj = realm.objects(NewsRealmPresentation.self)
        
        if obj.count > 100 {
            return true
        } else {
            return false
        }
        
    }
}
