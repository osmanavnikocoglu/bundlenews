//
//  BundleNewsResponse.swift
//  BundleNews
//
//  Created by Osman Avni Koçoğlu on 19.03.19.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//

import Foundation
struct BundleNewsResponse : Codable {
	let isSuccess : Bool?
	let errorMsg : String?
	let data : Data?

	enum CodingKeys: String, CodingKey {
		case isSuccess = "IsSuccess"
		case errorMsg = "ErrorMsg"
		case data = "Data"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
        isSuccess = try values.decodeIfPresent(Bool.self, forKey: .isSuccess)
        errorMsg = try values.decodeIfPresent(String.self, forKey: .errorMsg)
        data = try values.decodeIfPresent(Data.self, forKey: .data)
	}
}
