//
//  Items.swift
//  BundleNews
//
//  Created by Osman Avni Koçoğlu on 19.03.19.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//


import Foundation

struct Data : Codable {
	let items : [Items]?
	let followersCount : Int?

	enum CodingKeys: String, CodingKey {

		case items = "Items"
		case followersCount = "FollowersCount"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
        items = try values.decodeIfPresent([Items].self, forKey: .items)
        followersCount = try values.decodeIfPresent(Int.self, forKey: .followersCount)
	}

}
