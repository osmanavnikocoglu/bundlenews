//
//  Items.swift
//  BundleNews
//
//  Created by Osman Avni Koçoğlu on 19.03.19.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//

import Foundation

struct Items : Codable {
    let newsDetail : NewsDetail?
    
    enum CodingKeys: String, CodingKey {
        
        case newsDetail = "NewsDetail"
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        newsDetail = try values.decodeIfPresent(NewsDetail.self, forKey: .newsDetail)
    }
}
