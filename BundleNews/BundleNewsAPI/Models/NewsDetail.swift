//
//  NewsDetail.swift
//  BundleNews
//
//  Created by Osman Avni Koçoğlu on 20.03.19.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//

import Foundation

struct NewsDetail : Codable {
    let rssDataID : String?
	let channelCategoryLocalizationKey : String?
	let notificationChannelCategoryLocalizationKey : String?
	let newsChannelName : String?
	let title : String?
	let link : String?
	let content : String?
	let shareUrl : String?
	let pubDate : String?

	enum CodingKeys: String, CodingKey {
        case rssDataID = "RssDataID"
		case channelCategoryLocalizationKey = "ChannelCategoryLocalizationKey"
		case notificationChannelCategoryLocalizationKey = "NotificationChannelCategoryLocalizationKey"
		case newsChannelName = "NewsChannelName"
		case title = "Title"
		case link = "Link"
		case content = "Content"
		case shareUrl = "ShareUrl"
		case pubDate = "PubDate"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
        rssDataID = try values.decodeIfPresent(String.self, forKey: .rssDataID)
        channelCategoryLocalizationKey = try values.decodeIfPresent(String.self, forKey: .channelCategoryLocalizationKey)
        notificationChannelCategoryLocalizationKey = try values.decodeIfPresent(String.self, forKey: .notificationChannelCategoryLocalizationKey)
        newsChannelName = try values.decodeIfPresent(String.self, forKey: .newsChannelName)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        link = try values.decodeIfPresent(String.self, forKey: .link)
        content = try values.decodeIfPresent(String.self, forKey: .content)
        shareUrl = try values.decodeIfPresent(String.self, forKey: .shareUrl)
        pubDate = try values.decodeIfPresent(String.self, forKey: .pubDate)
	}

}
