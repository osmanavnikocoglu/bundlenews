//
//  AppContainer.swift
//  BundleNews
//
//  Created by Rooster on 19.03.2019.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//

import Foundation

let app = AppContainer()

final class AppContainer {
    
    let router = AppRouter()
    let service = BundleNewsService()
}
