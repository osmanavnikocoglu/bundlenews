//
//  NewsRealmPresentation.swift
//  BundleNews
//
//  Created by Rooster on 19.03.2019.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//
import Foundation
import RealmSwift

class NewsRealmPresentation: Object {
    
    @objc dynamic var title: String?
    @objc dynamic var detail: String?
    @objc dynamic var time: String?
    @objc dynamic var rssDataId: String?
    
    
    required convenience init(title: String?, detail: String?,time: String?,rssDataId: String?) {
        self.init()
        self.title = title
        self.detail = detail
        self.time = time
        self.rssDataId = rssDataId
    }
}
