//
//  NewsCell.swift
//  BundleNews
//
//  Created by Rooster on 19.03.2019.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//



import UIKit

class NewsCell: UITableViewCell {

    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.heightAnchor.constraint(equalToConstant: 135.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure (item: NewsPresentation) {
        titleLabel.text = item.title
        categoryLabel.text = item.detail?.localized
        timeLabel.text = item.time
    }
    
}

